module.exports = function(grunt) {
  'use strict';

  // require('time-grunt')(grunt);

  var requireDeps = ['main'];
  var files = grunt.file.expand({
    cwd: 'public'
  }, 'scripts/**/*.js');
  files.forEach(function(file) {
    requireDeps.push(file.replace('.js', ''));
  });

  var preprocessFiles = {
    'views/index.ejs': 'views/index.preprocess.ejs',
    'public/app.js': 'public/app.preprocess.js',
  };

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    preprocess: {
      // node线上模式
      pro: {
        options: {
          context: {
            DEBUG: false,
            JSP: false
          }
        },
        files: preprocessFiles
      },
      // java线上模式
      java: {
        options: {
          context: {
            DEBUG: false,
            JSP: true
          }
        },
        files: {
          '_java/index.jsp': 'views/index.preprocess.ejs',
          '_java/login.jsp': 'views/login.ejs',
        }
      },
      // node开发模式
      dev: {
        options: {
          context: {
            DEBUG: true,
            JSP: false
          }
        },
        files: preprocessFiles
      }
    },

    requirejs: {
      compile: {
        options: {
          mainConfigFile: '../kuyou/require.config.js',
          baseUrl: 'public',
          deps: requireDeps,
          name: 'common',
          create: true,
          out: 'public/dist/main.min.js',
          preserveLicenseComments: false
        }
      }
    },
    concat: {
      main: {
        src: [
          "public/lib/require.min.js",
          "public/dist/main.min.js",
        ],
        dest: "public/dist/main.min.js"
      }
    },
    copy: {
      main: {
        files: [{
          expand: true,
          cwd: '../kuyou/img',
          src: 'bg.jpg',
          dest: 'public/img'
        }, {
          expand: true,
          cwd: '../kuyou/img',
          src: ['**'],
          dest: 'public/dist/img'
        }]
      }
    },
    cssmin: {
      main: {
        files: {
          'public/dist/main.min.css': ['../kuyou/kuyou.css', 'public/main.css']
        }
      }
    },
    
    compress: {
      main: {
        options: {
          archive: '../<%= pkg.name %>.zip'
        },
        expand: true,
        cwd: './',
        src: ['**/**', '!**/node_modules/**', '!*.zip']
      }
    },
    shell: {
      multiple: {
        options: {
          failOnError: false,
          // stderr: false,
          execOptions: {
            cwd: '../'
          }
        },
        command: [
          'git clone https://jianghai@bitbucket.org/jianghai/kuyou.git',
          'git clone https://github.com/jianghai/vjs.git',
          'git clone -b old https://github.com/jianghai/trunk.git',
          'git clone https://github.com/jianghai/daterange.git',
          'git clone https://github.com/jianghai/jquery.extend.git'
        ].join(';')
      }
    },
    watch: {
      scripts: {
        files: ['**/*.preprocess.*'],
        tasks: ['preprocess:dev']
      },
    }
  });

  // These plugins provide necessary tasks.
  require('load-grunt-tasks')(grunt, {
    scope: 'devDependencies'
  });

  // Default task(s).
  grunt.registerTask('default', ['preprocess:pro', 'preprocess:java', 'requirejs', 'concat', 'copy', 'cssmin', 'compress']);
  grunt.registerTask('install', ['shell']);
  grunt.registerTask('dev', ['preprocess:dev']);
  grunt.registerTask('pro', ['preprocess:pro']);
}