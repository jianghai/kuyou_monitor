define([
  'jquery',
  'vjs'
], function($, vjs) {

  $.ajaxSetup({
    // timeout: 5000,
    complete: function(xhr) {
      if (xhr.getResponseHeader('sessionstatus') === 'timeout') {
        window.location.replace('login.html');
      }
    }
  });

  // @if !DEBUG
  var baseURL = '';
  // @endif
  
  // @if DEBUG
  var baseURL = 'http://192.168.9.126:9999/monitor/';
  // @endif 

  window.App = {

    dataURL: {
      topMenu: 'data/top_menu.json',
      sideMenu: 'data/side_menu.json',

      analysis_marketSize_line: baseURL + 'sightLine.do',
      // analysis_marketSize_line: 'data/list.json',
      analysis_marketSize_export: baseURL + 'sightLineExport.do',
      analysis_brandRanking_pie: baseURL + 'sightPie.do',
      analysis_brandRanking_grid: baseURL + 'sightPie2.do',
      analysis_brandRanking_circles: baseURL + 'sightPublicPraise.do',
      // analysis_brandRanking_circles: 'data/analysis_brandRanking_circles.json',
      analysis_brandRanking_tag: baseURL + 'tag.do',
      analysis_brandRanking_tag2: baseURL + 'tag2.do',
      analysis_brandRanking_export: baseURL + 'sightPieExport.do',

      analysis_detail_grid: baseURL + 'sightPieViewMore.do',
      analysis_detail_export: baseURL + 'sightPieExport.do',

      // analysis_trend_list: 'data/trend.json',
      analysis_trend_list: baseURL + 'phoneConfig.do',

      ranking_grid: baseURL + 'cup.do',
      ranking_export: baseURL + 'cupExport.do',

      product: baseURL + 'productDetail.do',
      product_tags: baseURL + 'productTag.do',
      product_fullLine: baseURL + 'productAvgPriceAndSalesVolume.do',
      product_special_grid_in: baseURL + 'productSellerDetailIn.do',
      product_special_grid_allOut: baseURL + 'productPlatformAllOut.do',
      product_special_grid_out: baseURL + 'productPlatformOut.do',
      product_special_line_in: baseURL + 'productPriceAndSalesVolumeIn.do',
      product_special_line_out: baseURL + 'productPriceOut.do',

      // checking_quadrant: 'data/quadrant.json',
      checking_quadrant: baseURL + 'productQuadrant.do',
      checking_export: baseURL + 'productQuadrantExport.do',
      // checking_quadrant_detail: 'quadrant_datail.json',
      checking_quadrant_detail: baseURL + 'productProperty.do',

      // compare_size: 'compare_size.json',
      compare_size: baseURL + 'allSize.do',
      // compare_brand: 'compare_brand.json',
      compare_brand: baseURL + 'allBrand.do',
      // compare_list: 'data/compare_list.json',
      compare_list: baseURL + 'brandList.do',
      // compare_setting_list: 'compare_setting_list.json',
      compare_setting_list: baseURL + 'contrastList.do',
      // compare_setting_param: 'compare_setting_param.json',
      compare_setting_param: baseURL + 'contrastParamList.do',
      compare_setting_save: baseURL + 'updateContrast.do',

      // compare_detail: 'data/compare_detail.json',
      compare_detail: baseURL + 'contrast.do',

      // iteration: 'data/iteration.json',
      iteration: baseURL + 'iteratorAnalysis.do',
      iteration_compare: baseURL + 'configParamter.do',
      // iteration_compare: 'data/iteration_compare.json'
      
      attention_brand: baseURL + 'allBrand.do',
      attention_platform: baseURL + 'allChannal.do',
      attention_emails: baseURL + 'viewAlarmEmail.do',
      attention_list: baseURL + 'alarmList.do',
      attention_list_delete: baseURL + 'delAlarmProduct.do',
      attention_record: baseURL + 'alarmRecord.do',
      attention_product_record: baseURL + 'productAlarmRecord.do',
      attention_edit: baseURL + 'editAlarmProduct.do',
      attention_edit_autocomplete: baseURL + 'searchProduct.do',
      attention_edit_preview: baseURL + 'informationByModelType.do',
      attention_edit_tao: baseURL + 'sellerIn.do',
      attention_edit_taoOut: baseURL + 'platformOut.do',
      attention_edit_taoOutShops: baseURL + 'sellerOfOutPlatform.do',
      attention_add_update: baseURL + 'saveOrUpdate.do',
    },

    echartsTheme: {
      textStyle: {
        fontFamily: 'arial, Microsoft Yahei'
      }
    },

    color: ['#2ec7c9', '#5ab1ef', '#ffb980', '#eea03b', '#eee55a', '#8e9cbc', '#b0ce6b', '#48a935', '#956d95', '#dc69aa', '#E0585A', '#333'],

    loading: $('#template-loading').html(),

    error: $('#template-error').html(),

    noData: vjs($('#template-noData').html()),
  };

  return App;
})