require([
  'jquery',
  'trunk',
  'app',
  'jquery.extend'
], function($, Trunk) {

  var AppView = Trunk.View.extend({

    el: $('body'),

    events: {
      'click .logo': 'onNavi',
      'click .category a': 'onNavi',
      'click .navi a': 'onNavi',
      'click .breadcrumbs a': 'onNavi',
      'mouseenter .text-overflow': 'onTextOverflow'
    },

    onNavi: function(e) {
      var href = e.target.href;
      if (href === location.href) {
        $(window).trigger('hashchange');
      }
    },

    onTextOverflow: function(e) {
      var target = $(e.target);
      if (target[0].offsetWidth < target[0].scrollWidth && !target.attr('title')) {
        target.attr('title', target.text());
      }
    },

    init: function() {

      var parent = this;

      // load svg document
      // var iconsContainer = $('<div>');
      // iconsContainer.css('display', 'none');
      // this.el.append(iconsContainer);
      // $.ajax({
      //   url: App.iconsURL,
      //   dataType: 'text'
      // }).done(function(res) {
      //   iconsContainer.html(res);
      // });

      this.content = $('#content');

      var TopMenu = Trunk.Model.extend({

        keyMap: {
          tv: {
            name: '电视',
            icon: 'tv'
          },
          phone: {
            name: '手机',
            icon: 'phone'
          },
          ac: {
            name: '空调',
            icon: 'aircon'
          },
          rf: {
            name: '冰箱',
            icon: 'fridge'
          },
          wm: {
            name: '洗衣机',
            icon: 'washer'
          },
          sa: {
            name: '小家电',
            icon: 'appliance',
            submenu: {
              ec: {
                name: '电饭煲'
              },
              ic: {
                name: '电磁炉'
              }
            }
          }
        },

        parse: function(res) {
          var _this = this;
          var _hash = location.hash.slice(1);

          var _keyMap = this.keyMap;
          $.each(res, function() {
            this.url = this.key;
            this.name = _keyMap[this.key].name;
            this.icon = _keyMap[this.key].icon;

            if (this.submenu) {
              var _subMap = _keyMap[this.key].submenu;
              var _url = this.url;
              $.each(this.submenu, function() {
                this.url = _url + '/' + this.key;
                this.name = _subMap[this.key].name;
              });
            }
          });
          return res;
        },

        onFetch: function(res) {
          this.reset({
            menu: res
          });
        },

        fetch: function() {
          var _this = this;
          $.ajax({
            url: App.dataURL.topMenu,
            dataType: 'json'
          }).done(function(res) {
            _this.onFetch(_this.parse(res));
          });
        }
      });

      var TopMenuView = Trunk.View.extend({

        model: TopMenu,

        el: '.top-menu',

        template: '.template-topMenu',

        afterRender: function() {
          this.highlight();
        },

        highlight: function() {
          if (!this.model.data.menu) return;
          var _el = this.el;
          _el.find('.active').removeClass('active');
          $.each(App.topUrl.split('/'), function(i, v) {
            _el = _el.find('[data-key="' + v + '"]').addClass('active');
          });
        }
      });

      var SideMenu = Trunk.Model.extend({

        keyMap: {
          analysis: {
            name: '市场把脉',
            icon: 'sight',
            submenu: {
              trend: {
                name: '配置演化'
              },
              detail: {
                name: '品牌排名'
              }
            }
          },
          checking: {
            name: '产品分析',
            icon: 'log'
          },
          iteration: {
            name: '迭代分析',
            icon: 'tag'
          },
          ranking: {
            name: '产品排行',
            icon: 'cup'
          },
          compare: {
            name: '竞品对比',
            icon: 'balance',
            submenu: {
              detail: {
                name: '详情'
              }
            }
          },
          attention: {
            name: '重点关注',
            icon: 'star',
            submenu: {
              add: {
                name: '添加产品'
              },
              edit: {
                name: '编辑产品'
              },
              list: {
                name: '报警列表'
              }
            }
          },
          product: {
            name: '产品详情'
          },
          test: {
            name: '测试'
          }
        },

        cache: {},

        parse: function(res) {
          var _keyMap = this.keyMap;
          $.each(res, function() {
            this.url = App.topUrl + '/' + this.key;
            this.name = _keyMap[this.key].name;
            this.icon = _keyMap[this.key].icon;
            if (this.submenu) {
              var _subMap = _keyMap[this.key].submenu;
              var _url = this.url;
              $.each(this.submenu, function() {
                this.url = _url + '/' + this.key;
                this.name = _subMap[this.key].name;
              });
            }
          });
          return res;
        },

        onFetch: function(res) {
          this.cache[App.category] = res;
          this.reset({
            menu: res
          });
        },

        fetch: function() {
          var _this = this;
          $.ajax({
            url: App.dataURL.sideMenu,
            data: {
              category: App.category
            },
            dataType: 'json'
          }).done(function(res) {
            _this.onFetch(_this.parse(res));
          });
        }
      });

      var SideMenuView = Trunk.View.extend({

        model: SideMenu,

        el: this.$('.side-menu'),

        template: '.template-sideMenu',

        show: function() {
          var _menu = this.model.cache[App.category];
          if (!_menu) {
            this.model.fetch();
          } else {
            this.model.reset({
              menu: _menu
            });
          }
        },

        afterRender: function() {
          this.highlight();
        },

        highlight: function() {
          var _el = this.el;
          _el.find('.active').removeClass('active');
          $.each(App.sideUrl.split('/'), function(i, v) {
            _el = _el.find('[data-key="' + v + '"]').addClass('active');
          });
        }
      });



      var Breadcrumbs = Trunk.Model.extend({

        getNodes: function() {
          // debugger;
          var _nodes = [];
          var _keyMapTop = parent.topMenu.model.keyMap;
          var _keyMapSide = parent.sideMenu.model.keyMap;


          var _url = [];
          var _map = _keyMapTop;
          var hash = location.hash.slice(1).split('?')[0].split('/');
          var i = 0;
          var splitIndex = 0;
          var key = hash[0] || Object.keys(_map)[0];
          while (key) {

            i++;

            _url.push(key);
            var _node = {
              name: _map[key].name,
              url: _url.join('/')
            };
            _nodes.push(_node);

            if (_keyMapSide) {
              App.category = key;
            }

            if (_map[key].submenu) {
              _map = _map[key].submenu;
              if (!hash[i] && !_keyMapSide) {
                _map = null;
              }
            } else {
              if (!splitIndex) {
                splitIndex = i;
              }
              _map = _keyMapSide;
              _keyMapSide = null;
            }

            key = hash[i] || _map && Object.keys(_map)[0];
          }

          App.url = _url.join('/');
          App.topUrl = _url.slice(0, splitIndex).join('/');
          App.sideUrl = _url.slice(splitIndex, _url.length).join('/');

          return _nodes;
        }
      });

      var BreadcrumbsView = Trunk.View.extend({

        model: Breadcrumbs,

        el: this.$('.breadcrumbs'),

        template: '.template-breadcrumbs',

        show: function() {
          this.model.reset({
            nodes: this.model.getNodes()
          });
        }
      });

      this.topMenu = new TopMenuView();
      this.sideMenu = new SideMenuView();
      this.breadcrumbs = new BreadcrumbsView();

      this.childViews = {
        topMenu: this.topMenu
      };
    }
  });

  var Router = Trunk.Router.extend({

    router: {
      '*/attention/add': 'attentionEdit',
      '*/attention/edit': 'attentionEdit',
      '*': 'show'
    },

    attentionEdit: function(topUrl) {
      this.show(topUrl + '/attention/edit');
    },

    show: function(dir) {

      // app.breadcrumbs.show(hash);
      app.breadcrumbs.show();

      app.sideMenu.show();

      app.topMenu.highlight();

      var _dir = dir.slice(App.topUrl.length + 1);

      _dir || (_dir = App.sideUrl);

      $.get('html/' + _dir + '.html', function(str) {
        var id = _dir.replace(/\//g, '-');
        app.content.html(str)
          .attr('class', 'content ' + id);
        require(['scripts/' + _dir], function(View) {
          var view = new View({
            el: app.content
          });
          view.show();
          // if (_dir !== App.sideUrl) return;
          // var view = new View();
          // app.content.html(view.el);

          // if (!view.html) {
          //   $.get('html/' + _dir + '.html', function(str) {
          //     // if (_dir !== App.sideUrl) return;
          //     View.prototype.html = str;
          //     view.show();
          //   });
          // } else {
          //   view.show();
          // }
        });
      });

      // require(['scripts/' + _dir], function(View) {

      //   // if (_dir !== App.sideUrl) return;
      //   var view = new View();
      //   app.content.html(view.el);

      //   if (!view.html) {
      //     $.get('html/' + _dir + '.html', function(str) {
      //       // if (_dir !== App.sideUrl) return;
      //       View.prototype.html = str;
      //       view.show();
      //     });
      //   } else {
      //     view.show();
      //   }
      // });
    }
  });

  var app = new AppView();
  app.show();

  var router = new Router();

});