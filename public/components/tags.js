define([
  'jquery',
  'base'
], function($, base) {

  var Model = base.Model.extend({

    range: function(data, range) {
      var dataScall = data[1] - data[0],
        rangeScall = range[1] - range[0];
      return function(v) {
        return +range[0] + (dataScall ? rangeScall * (v - data[0]) / dataScall : 0);
      };
    },

    parse: function(data) {
      var _this = this;

      // if (!this.RGB) {
      this.RGB = [
        [],
        [],
        []
      ];
      $.each(this.colorRange, function(i) {
        var rgb = _this.colorRange[i];
        if (rgb.length === 4) {
          rgb += rgb.slice(1);
        }
        $.each(_this.RGB, function(i) {
          i = i + i + 1;
          this.push(parseInt(rgb.slice(i, i + 2), 16));
        });
      });
      // }

      var countArr = [];
      $.each(data, function() {
        countArr.push(this.count);
      });
      var countRange = [Math.min.apply(null, countArr), Math.max.apply(null, countArr)];

      $.each(data, function() {
        var _color = '#';
        var _count = this.count;
        $.each(_this.RGB, function() {
          var val = Math.ceil(_this.range(countRange, this)(_count)).toString(16);
          if (!val[1]) val = '0' + val;
          _color += val;
        });
        this.color = _color;
        this.size = Math.ceil(_this.range(countRange, _this.sizeRange)(_count));
      });

      return data;
    },

    onFetch: function(res) {
      this.reset({
        tags: this.parse(res)
      });
    }
  });

  var View = base.View.extend({

    model: Model,

    template: '#template-tags'
  });

  return View;
});