define([
  'jquery',
  'base',
  'app',
  'highcharts',
], function($, base, app) {

  var Model = base.Model.extend({

  });

  var View = base.View.extend({

    model: Model,

    events: {
      'mouseenter .circle-container': 'onMouseenter',
      'mouseleave .circle-container': 'onMouseleave',
      'click .circle-container': 'onSelect'
    },

    onMouseenter: function(e) {
      var target = $(e.currentTarget);
      // if (target.hasClass('disabled')) return;
      if (target.hasClass('active')) return;
      var index = target.index();
      target.find('.name').css({
        color: '#fff',
        background: App.color[index]
      });
    },

    onMouseleave: function(e) {
      var target = $(e.currentTarget);
      if (target.hasClass('active')) return;
      target.find('.name').removeAttr('style');
    },

    onSelect: function(e) {
      this.select($(e.currentTarget));
    },

    select: function(target) {
      target.addClass('active').find('.name').addClass('icon-ok');
      target.siblings().removeClass('active').find('.name').removeClass('icon-ok').removeAttr('style');
      this.trigger('select', target.attr('data-name'));
    },

    render: function() {
      this.container = this.$('.circles-container');
      var _this = this;

      this.delegateEvents();

      this.el.html(this.template(this.model.data));

      this.$('.circle').each(function(i) {

        var store = _this.model.data.stores[i];

        var option = {
          chart: {
            events: {
              click: function() {
                _this.select.call(_this, $(this.container).closest('.circle-container'));
              }
            }
          },
          title: {
            align: 'center',
            verticalAlign: 'middle',
            style: {
              fontSize: '12px'
            },
            y: -5
          },
          plotOptions: {
            pie: {
              dataLabels: {
                enabled: false
              },
              events: {
                click: function() {
                  _this.select.call(_this, $(this.chart.container).closest('.circle-container'));
                }
              }
            }
          },
          credits: {
            enabled: false
          },
          colors: [app.color[i], '#ebebeb'],
          tooltip: {
            pointFormat: '{series.name}: <b>{point.y}条</b>'
          },
          series: [{
            type: 'pie',
            name: store.brand,
            innerSize: '70%',
            data: []
          }]
        };

        var percent = $.format.percent(store.praise / (store.praise + store.badReview), 2);
        option.title.text = '好评<br>' + percent;
        option.color = [App.color[i], '#ebebeb'];
        option.series[0].data = [
          ['好评', store.praise || 0],
          ['差评', store.badReview || 0]
        ];
        if (!store.praise && !store.badReview) {
          // debugger;
          option.title.text = '暂无数据';
          // option.tooltip.show = false;
          // option.series[0].data = [{
          //   name: '',
          //   value: 0
          // }, {
          //   name: '',
          //   value: 1
          // }];
          $(this).parent().addClass('disabled');
        }
        $(this).highcharts(option);
      });
    }
  });

  return View;
});