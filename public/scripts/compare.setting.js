define([
  'jquery',
  'trunk',
  'base',
  // 'success',
  'multiSelect'
], function($, Trunk, base, MultiSelect) {

  var View = base.View.extend({

    modelProperty: {
      url: App.dataURL.compare_setting_list,
      param: {
        category: App.category
      }
    },

    tag: 'div',

    template: '#template_compare_setting',

    className: 'compare-setting module',

    events: {
      'click .save': 'onSave'
    },

    onSave: function(e) {

      var self = this;

      var target = $(e.target);

      var isValid = true;
      this.groups.each(function(model) {
        if (!model.set({
            name: model.view.$('.input-text').eq(0).val(),
            items: model.data.items
          })) {
          isValid = false;
        }
      });

      if (!isValid) {
        target.prev().show().text('组名和对比项不能为空');
        return;
      }
      target.prev().hide();
      target.text('保存中...');
      $.ajax({
        type: 'POST',
        url: App.dataURL.compare_setting_save,
        data: {
          data: JSON.stringify(this.groups.toArray()),
          category: App.category
        }
      }).done(function(res) {
        if (res.success) {
          self.dialog.close();
          // success.model.reset({
          //   tip: '保存成功'
          // });
        }
      });
    },

    addOne: function(model) {
      var view = new this.GroupView({
        model: model
      });
      var prev = model.prev();
      if (prev) {
        prev.view.el.after(view.el);
      } else {
        this.list.append(view.el);
      }
    },

    afterRender: function() {
      var self = this;
      this.list = this.$('.list');
      this.groups.clear();
      $.each(this.model.data || [], function() {
        self.groups.add(this);
      });
    },

    init: function() {

      var Group = Trunk.Model.extend({

        rules: {
          name: function(v) {
            return !!v;
          },

          items: function(v) {
            return !!v && !!v.length;
          }
        },

        validate: function(data) {
          var _rules = this.rules;
          var res = {};
          $.each(data, function(k, v) {
            if (_rules[k]) {
              if (!_rules[k](v))
                res[k] = 1;
            }
          });
          return res;
        },
      });

      var Groups = Trunk.Models.extend({

        model: Group
      });

      this.GroupView = Trunk.View.extend({

        model: Group,

        tag: 'li',

        template: '#template_compare_group',

        events: {
          'click .operate .add': 'onAdd',
          'click .operate .remove': 'onRemove',
          'click .name': 'onNameEdit',
          'click .drop-toggle': 'onDrop',
          'blur .input-name': 'onNameBlur'
        },

        onValidate: function() {
          this.$('input').removeClass('error');
        },

        onInvalid: function(res) {
          res.name && this.$('.input-text').eq(0).addClass('error');
          res.items && this.$('.input-text').eq(1).addClass('error');
        },

        onNameBlur: function(e) {
          var target = $(e.target);
          var val = target.val();
          if (!val) return target.focus();
          this.model.set({
            name: val
          });
          target.prev().text(val).parent().removeClass('edit');
        },

        onNameEdit: function(e) {
          var target = $(e.target);
          if (target.hasClass('fixed')) return false;
          target.parent().addClass('edit');
        },

        onAdd: function() {
          this.model.after();
        },

        onRemove: function() {
          this.model.remove();
        },

        onDrop: function() {
          var _this = this;
          if (!this.select) {
            this.select = new MultiSelect({
              modelProperty: {
                selected: this.model.data.items,
                param: {
                  category: App.category
                },
                url: App.dataURL.compare_setting_param
              },
              el: this.$('.select-list')
            });
            this.select.show();

            this.select.on('change', function(selected) {
              var _items = Object.keys(selected);
              _this.model.data.items = _items;
              _this.childViews.items.model.reset({
                items: _items
              })
            });
          }
        },

        init: function() {

          var _this = this;

          this.childViews = {

            items: new ItemsView({
              modelProperty: {
                defaults: {
                  items: this.model.data.items
                }
              }
            })
          };

          this.childViews.items.on('remove', function(item) {
            _this.model.data.items = $.grep(_this.model.data.items, function(v) {
              return v !== item
            });

            if (_this.select) {
              delete _this.select.model.selected[item];
              _this.select.show();
            }
          });

          this.render();
        }
      });

      var ItemsView = Trunk.View.extend({

        model: Trunk.Model.extend(),

        el: '.items',

        events: {
          'click .remove': 'onRemove'
        },

        onRemove: function(e) {
          var target = $(e.currentTarget);
          target.parent().remove();
          this.trigger('remove', target.attr('data-item'));
        },

        template: '#template_compare_group_items'
      });

      this.groups = new Groups();

      this.listen(this.groups, 'add', this.addOne);
      // this.listen(this.groups, 'change', this.onGroupsChange);
    }
  });

  return View;
});