define(['d3'], function(d3) {



  function create(setting) {
    var option = {
      padding: [20, 20],
      sliderMargin: [10, 10],
      sliderSize: [6, 30],
      zoomSize: [14, 14],
      zoomDragSize: [14, 6],
      magnification: 8
    };

    $.extend(true, option, setting);

    var _el = $(option.el);
    option.width = _el.width() || 960;
    option.height = _el.height() || option.width * .5;


    var halfWidth = option.width / 2;
    var halfHeight = option.height / 2;
    var realWidth = option.width - option.padding[0] * 2;
    var realHeight = option.height - option.padding[1] * 2;
    var halfRealWidth = realWidth / 2;
    var halfRealHeight = realHeight / 2;

    var svg = d3.select(option.el).append("svg")
      .attr('width', option.width)
      .attr('height', option.height)
      .append("g");

    var zoomGroups = svg.selectAll('svg').data([1, 2, 3, 4]).enter().append('svg').attr('x', function(d, i) {
      return i === 0 || i === 3 ? halfWidth : option.padding[0];
    }).attr('y', function(d, i) {
      return i === 2 || i === 3 ? halfHeight : option.padding[1];
    }).attr('width', halfRealWidth).attr('height', halfRealHeight);


    zoomGroups.each(function(d, i) {
      var _zoomBehavior = d3.behavior.zoom().scaleExtent([1, option.magnification])
        .on("zoom", zoom);
      var _svg = d3.select(this).call(_zoomBehavior);

      _svg.append("rect")
        .attr("class", "overlay")
        .attr("width", halfRealWidth - 1)
        .attr("height", halfRealHeight - 1);

      // background grid
      _svg.append('g')
        .attr('class', 'line')
        .selectAll('line').data(d3.range(0, halfRealWidth, halfRealWidth / 5)).enter()
        .append('line')
        .attr('x1', function(d) {
          return d;
        })
        .attr('y1', 0)
        .attr('x2', function(d) {
          return d;
        })
        .attr('y2', halfRealHeight)
      _svg.append('g')
        .attr('class', 'line')
        .selectAll('line').data(d3.range(0, halfRealHeight, halfRealHeight / 5)).enter()
        .append('line')
        .attr('x1', 0)
        .attr('y1', function(d) {
          return d;
        })
        .attr('x2', halfRealWidth)
        .attr('y2', function(d) {
          return d;
        })

      var _data = option.series[i];

      if (_data.length) {

        var _domainX, _domainY;

        if (i === 0) {
          _domainX = [option.xDivider[1], option.xRange[1]];
          _domainY = [0, option.yRange[0]];
        }
        if (i === 1) {
          _domainX = [0, option.xDivider[0]];
          _domainY = [0, option.yRange[0]];
        }
        if (i === 2) {
          _domainX = [0, option.xDivider[0]];
          _domainY = [option.yRange[1], 0];
        }
        if (i === 3) {
          _domainX = [option.xDivider[1], option.xRange[1]];
          _domainY = [option.yRange[1], 0];
        }
        var _xScale = d3.scale.linear().domain(_domainX).range([30, halfRealWidth - 30]);

        var _yScale = d3.scale.linear().domain(_domainY).range([halfRealHeight - 20, 20]);

        _svg.append('g').attr('class', 'circles')
          .selectAll("circle")
          .data(_data)
          .enter().append("circle")
          .on('mouseover', function() {
            // d3.select(this).style('fill-opacity', 1);
            option.mouseover.apply(this, arguments);
          })
          .on('mousemove', option.mousemove)
          .on('mouseout', function() {
            // d3.select(this).style('fill-opacity', .5);
            option.mouseout.apply(this, arguments);
          })
          .on('click', option.click)
          .attr("cx", function(d) {
            return _xScale(d.x);
          })
          .attr("cy", function(d) {
            return _yScale(d.y);
          })
          .attr("r", 10)
          .attr("data-model", function(d) {
            return d.modelType;
          })
          .attr('class', function(d) {
            if (d.brand === 'TCL') return 'highlight'
          })
          .transition().duration(function() {
            return Math.ceil(Math.random() * 3000);
          })
          .style('fill-opacity', .5);
      }

      var _margin = option.sliderMargin.concat();
      if (i === 0 || i === 3) {
        _margin[0] = halfRealWidth - option.sliderMargin[0] - option.zoomSize[0];
      }
      if (i === 2 || i === 3) {
        _margin[1] = halfRealHeight - option.sliderMargin[1] - option.zoomSize[1] * 2 - option.sliderSize[1];
      }
      var _zoomControl = _svg.append('g')
        .attr('class', 'zoom-control')
        .attr('transform', 'translate(' + [_margin[0] - 0.5, _margin[1] - 0.5] + ')')
        .on('dblclick', function() {
          d3.event.stopPropagation();
        })

      var _slider = _zoomControl.append('g')
        .attr('transform', 'translate(0, ' + option.zoomSize[1] + ')');
      _slider.append('rect')
        .attr('class', 'slider')
        .attr('x', (option.zoomSize[0] - option.sliderSize[0]) / 2)
        .attr('width', option.sliderSize[0])
        .attr('height', option.sliderSize[1]);
      var bar = _slider.append('rect')
        .attr('class', 'slider-bar')
        .attr('x', (option.zoomSize[0] - option.sliderSize[0]) / 2)
        .attr('width', option.sliderSize[0])
        .attr('height', option.sliderSize[1]);

      var scale = d3.scale.linear().domain([option.sliderSize[1] - option.zoomDragSize[1], 0]).range([1, option.magnification]);

      _slider.append('rect')
        .attr('class', 'slider-handle')
        .attr('y', option.sliderSize[1] - option.zoomDragSize[1])
        .attr('width', option.zoomDragSize[0])
        .attr('height', option.zoomDragSize[1])
        .attr('rx', option.zoomDragSize[1] * .3)
        .call(d3.behavior.drag().on('dragstart', function() {
          d3.event.sourceEvent.stopPropagation();
        }).on('drag', function() {
          var y = d3.event.y;
          var _half = option.zoomDragSize[1] / 2;
          if (y <= option.sliderSize[1] - _half && y >= _half) {
            scaleTo(_zoomBehavior, _svg, scale(y - _half));
          }
        }));

      _zoomControl.selectAll('g.zoom-in-out').data([1, 2]).enter().append('g')
        .attr('class', 'zoom-in-out').each(function(dd, j) {
          var _this = d3.select(this);
          var _use = 'plus';
          if (j === 1) {
            _use = 'minus';
            _this.attr('transform', 'translate(0, ' + (option.zoomSize[1] + option.sliderSize[1]) + ')');
          }
          _this.on('click', function() {
            var _scale = _zoomBehavior.scale();
            if (j === 1) {
              if (_scale === 1) return;
              _scale -= .5;
            } else {
              if (_scale === option.magnification) return;
              _scale += .5;
            }
            if (_scale > option.magnification) {
              _scale = option.magnification;
            }
            if (_scale < 1) {
              _scale = 1;
            }
            scaleTo(_zoomBehavior, _svg, _scale);
            return false;
          });
          _this.append('rect')
            .attr('width', option.zoomSize[0])
            .attr('height', option.zoomSize[1])
            .attr('rx', option.zoomSize[0] * .15)
          _this.append('use')
            .attr('xlink:href', '#icon-' + _use)
            .attr('x', option.zoomSize[0] * .15)
            .attr('y', option.zoomSize[1] * .15)
            .attr('width', option.zoomSize[0] * .7)
            .attr('height', option.zoomSize[1] * .7)
        });
    });


    function zoom() {
      var _this = d3.select(this);
      setBar(_this, d3.event.scale);

      var t = d3.event.translate,
        s = d3.event.scale;
      // console.log(halfRealWidth, t[0]);
      // t[0] = Math.min(0, Math.max(halfRealWidth * (1 - s), t[0]));
      // t[1] = Math.min(0, Math.max(halfRealHeight * (1 - s), t[1]));
      // t[1] = Math.min(halfRealHeight / 2 * (2 * s - 1), Math.max(halfRealHeight / 2 * (1 - s) - halfRealHeight / 2 * s, t[1]));
      t[0] = Math.min(halfRealWidth / 2 * (2 * s - 1), Math.max(halfRealWidth / 2 * (1 - s) - halfRealWidth / 2 * s, t[0]));
      t[1] = Math.min(halfRealHeight / 2 * (2 * s - 1), Math.max(halfRealHeight / 2 * (1 - s) - halfRealHeight / 2 * s, t[1]));
      _this.select('.circles').attr("transform", "translate(" + t + ")scale(" + s + ")")
        .selectAll('circle')
        .attr('r', function() {
          return 10 / s;
        })
    }

    function scaleTo(zoomBehavior, target, scale) {
      s = zoomBehavior.scale(),
        tx = zoomBehavior.translate()[0],
        ty = zoomBehavior.translate()[1],
        sReal = scale / s,
        dtx = realWidth / 4 * (1 - sReal),
        dty = realHeight / 4 * (1 - sReal),
        txNew = sReal * tx + dtx,
        tyNew = sReal * ty + dty;

      zoomBehavior.translate([txNew, tyNew]).scale(scale).event(target);
    }

    var setBarScale = d3.scale.linear().domain([1, option.magnification]).range([option.sliderSize[1] - option.zoomDragSize[1], 0]);

    function setBar(svg, scale) {
      var y = setBarScale(scale);
      svg.select('.slider-handle').attr('y', y);
      svg.select('.slider-bar').attr('height', y);
    }



    var _axis = svg.selectAll('g.axis').data([0, 1]).enter().append('g')
      .attr('class', function(d, i) {
        return 'axis axis-' + (i ? 'y' : 'x');
      })
      .attr('transform', function(d, i) {
        return 'translate(' + [i ? halfWidth : 0, i ? 0 : halfHeight] + ')'
      });

    _axis.append('line')
      .attr('x2', function(d, i) {
        return i ? 0 : option.width
      })
      .attr('y2', function(d, i) {
        return i ? option.height : 0
      });
    var _labels = _axis.selectAll('g.axis-label').data([0, 1]).enter().append('g')
      .attr('class', 'axis-label')
      .attr('transform', function(d, i, j) {
        return 'translate(' + [j === 0 && i === 1 ? option.width - 75 : 0, j === 1 && i === 1 ? option.height - 75 : 0] + ')';
      });
    _labels.append('rect')
      .attr('x', function(d, i, j) {
        if (j === 1) return -10;
      })
      .attr('y', function(d, i, j) {
        if (j === 0) return -9;
      })
      .attr('rx', 3)
      .attr("width", function(d, i, j) {
        return j === 0 ? 75 : 20;
      })
      .attr("height", function(d, i, j) {
        return j === 0 ? 20 : 75;
      });
    _labels.each(function(d, i, j) {
      var _this = d3.select(this);
      if (j === 0) {
        _this.append('text')
          .attr('x', 5)
          .attr('y', 4)
          .text('销量占比');
        _this.append('use')
          .attr('xlink:href', function() {
            return '#icon-' + (!i ? 'minus' : 'plus')
          })
          .attr('width', 9)
          .attr('height', 9)
          .attr('x', 58)
          .attr('y', -5)
      }
      if (j === 1) {
        _this.append('text').selectAll('tspan').data('销量增幅'.split('')).enter()
          .append('tspan')
          .attr('x', -7)
          .attr('dy', 14)
          .text(function(d) {
            return d;
          });
        _this.append('use')
          .attr('xlink:href', function() {
            return '#icon-' + (i ? 'minus' : 'plus')
          })
          .attr('width', 9)
          .attr('height', 9)
          .attr('x', -5)
          .attr('y', 62)
      }
    });
  }

  return create;
});