define([
  'jquery',
  'trunk',
  'search',
  'grid',
  'dialog'
], function($, Trunk, Search, Grid, Dialog) {

  var View = Trunk.View.extend({

    init: function() {

      var search = new Search({
        modelProperty: {
          defaults: {
            placeholder: '在此输入品牌'
          }
        }
      });

      var list = new Grid({

        modelProperty: {
          url: App.dataURL.iteration,
          param: {
            limit: 2
          }
        },

        events: {
          'click .prev': 'onPrev',
          'click .next': 'onNext',
          'click .compare': 'onCompare'
        },

        onCompare: function(e) {
          this.compare.model.setParam({
            start: 0,
            ids: $(e.currentTarget).parent().next().find('li').map(function() {
              return $(this).attr('data-id');
            }).get()
          });
        },

        onPrev: function(e) {
          if (this.clicked) return;
          this.clicked = true;
          var target = $(e.currentTarget);
          var line = target.closest('.line');
          var count = line.attr('data-count');
          var index = line.data('index') || 0;
          line.next().find('ul').css('margin-left', '+=293px');
          var self = this;
          setTimeout(function() {
            self.clicked = false;
          }, 300);
          line.data('index', --index);
          target.next().prop('disabled', false);
          target.prop('disabled', !index);
        },

        onNext: function(e) {
          if (this.clicked) return;
          this.clicked = true;
          var target = $(e.currentTarget);
          var line = target.closest('.line');
          var count = line.attr('data-count');
          var index = line.data('index') || 0;
          line.next().find('ul').css('margin-left', '-=293px');
          var self = this;
          setTimeout(function() {
            self.clicked = false;
          }, 300);
          line.data('index', ++index);
          target.prev().prop('disabled', false);
          target.prop('disabled', index === count - 3);
        },

        el: '.list',

        template: '.template-list',

        init: function() {
          this.compare = new Grid({

            modelProperty: {
              url: App.dataURL.iteration_compare,
              param: {
                limit: 5
              },
              cols: [{
                "col": "name",
                "name": "产品名称"
              }, {
                "col": "CPU核数"
              }, {
                "col": "CPU频率"
              }, {
                "col": "运行内存RAM"
              }, {
                "col": "机身内存ROM"
              }, {
                "col": "最大存储扩展"
              }, {
                "col": "分辨率"
              }, {
                "col": "外形尺寸"
              }, {
                "col": "屏幕尺寸"
              }]
            },
            tag: 'div',
            className: 'iteration-compare module',
            template: '#template_iteration_compare'
          });

          this.dialog = new Dialog({
            wapper: $('#content'),
            modelProperty: {
              defaults: {
                title: '参数对比'
              }
            },
            child: this.compare
          });
        }
      });

      search.on('search', function(key) {
        list.model.setParam({
          start: 0,
          brand: key
        });
      });

      this.childViews = {
        search: search,
        list: list
      };
    }
  });

  return View;
});