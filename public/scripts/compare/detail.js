define([
  'jquery',
  'trunk',
  'base'
], function($, Trunk, base) {

  var View = Trunk.View.extend({

    init: function() {

      var Detail = base.Model.extend({

        url: App.dataURL.compare_detail,

        param: {
          category: App.category
        },

        onFetch: function(res) {
          var _this = this;
          $.each(res.stores, function() {
            $.extend(this.base, _this.items[this.base.id]);
            // this.base.options = this.group.shift().options;
          });
          var n = res.stores.length;
          while (n < 4) {
            res.stores.push({});
            n++;
          }
          this.reset(res);
        },

        init: function() {
          var _this = this;
          this.items = {};
          $.each(JSON.parse(localStorage.getItem('compareItems')) || [], function() {
            _this.items[this.id] = this;
          });
          this.param.ids = Object.keys(this.items).join(',');
        }
      });

      var DetailView = base.View.extend({

        model: Detail,

        el: '.detail',

        template: '.template-detail',

        events: {
          'click dt': 'onToggle'
        },

        onToggle: function(e) {
          $(e.currentTarget).parent().toggleClass('unfold');
        }
      });

      var detail = new DetailView();

      this.childViews = {

        detail: detail
      };
    }
  });

  return View;
});