define([
  'jquery',
  'trunk',
  'base',
  'datepicker',
  'components/tags',
  'grid',
  'highcharts.line',
  'tab',
], function($, Trunk, base, Datepicker, Tags, Grid, Line) {

  var View = Trunk.View.extend({

    init: function() {

      var datepicker = new Datepicker();

      var publicParam = $.extend({
        category: App.category
      }, Trunk.get);

      var Detail = base.Model.extend({

        url: App.dataURL.product,

        param: publicParam
      });

      var DetailView = base.View.extend({

        model: Detail,

        el: '.detail',

        template: '.template-detail',

        events: {
          'click .tab-item': 'onTab',
        },

        onTab: function(e) {
          this.type = $(e.target).attr('data-type');
          var _tags = this.childViews.tags;
          _tags.model.colorRange = this.type === 'good' ? ['#ffb980', '#e60012'] : ['#8e9cbc', '#0fabad'];
          _tags.model.setParam({
            type: this.type
          });
        },

        init: function() {

          this.tags = new Tags({
            modelProperty: {
              url: App.dataURL.product_tags,
              param: $.extend({
                limit: 10,
                type: 'good',
                begin: datepicker.getBegin(),
                end: datepicker.getEnd()
              }, publicParam),
              colorRange: ['#ffb980', '#e60012'],
              sizeRange: [12, 18]
            },
            el: '.tags-content'
          });

          this.childViews = {

            tags: this.tags
          };
        }
      });

      var SpecialView = Trunk.View.extend({

        model: Trunk.Model.extend({
          defaults: {
            name: '天猫',
            cid: 'Mtmall'
          }
        }),

        el: '.special',

        template: '.template-special',

        events: {
          'click .tab-item': 'onTab',
          'click .back-out': 'onBack',
          'click .show-store': 'onShowStore',
          'click .show-stores': 'onShowStores',
        },

        onShowStore: function(e) {
          var target = $(e.target);
          this.store.isActive = true;
          this.el.append(this.store.el);
          this.store.model.reset({
            cid: target.attr('data-cid'),
            iid: target.attr('data-iid'),
            name: target.attr('data-name')
          });
          return false;
        },

        onShowStores: function(e) {
          var target = $(e.target);
          var _grid = this.childViews.grid;
          _grid.model.cols1Last = _grid.model.cols1.pop();
          this.model.reset({
            name: target.attr('data-name'),
            cid: target.attr('data-cid')
          });
          return false;
        },

        onBack: function() {
          var _grid = this.childViews.grid;
          _grid.model.cols1.push(_grid.model.cols1Last);
          this.model.reset({
            name: '淘系外',
            cid: null
          });
        },

        onTab: function(e) {
          var target = $(e.currentTarget);
          this.model.reset({
            name: target.text(),
            cid: target.attr('data-key')
          });
        },

        init: function() {
          var _this = this;

          var StoreView = Trunk.View.extend({

            model: Trunk.Model.extend(),

            tag: 'div',

            template: '#template_product_special_store',

            className: 'store',

            events: {
              'click .close': 'onClose'
            },

            onClose: function() {
              this.el.remove();
              this.isActive = false;
            },

            init: function() {

              var _this = this;

              this.isActive = false;

              this.line = new Line({

                show: function() {
                  var cid = this.parent.model.data.cid;

                  if (cid in {
                      'Mtmall': '',
                      'Mtaobao': ''
                    }) {
                    this.model.url = this.model.url1;
                    this.model.cols = this.model.cols1;
                    this.option.series = this.series1;
                  } else {
                    this.model.url = this.model.url2;
                    this.model.cols = this.model.cols2;
                    this.option.series = this.series2;
                  }
                  this.model.setParam({
                    begin: datepicker.getBegin(),
                    end: datepicker.getEnd(),
                    cid: cid,
                    iid: this.parent.model.data.iid
                  });
                },

                modelProperty: {
                  url1: App.dataURL.product_special_line_in,
                  url2: App.dataURL.product_special_line_out,
                  param: publicParam,
                  group: 'time',
                  cols1: [{
                    col: 'nowPrice',
                    name: '价格'
                  }, {
                    col: 'salesVolume',
                    name: '销量'
                  }],
                  cols2: [{
                    col: 'nowPrice',
                    name: '价格'
                  }],
                  getSingleData: function(col, store) {
                    var res = {
                      y: store[col]
                    };
                    if (col === 'nowPrice') {
                      res.promotion = store.promotion;
                      if (res.promotion) {
                        res.marker = {
                          fillColor: '#FF9BA3',
                          lineColor: '#dd515c',
                          lineWidth: 2,
                          radius: 6,
                          // symbol: 'circle'
                        }
                      }
                    }
                    return res;
                  }
                },
                option: {
                  tooltip: {
                    formatter: function() {
                      var promotion = this.points[0].point.promotion;
                      var tip = this.x + '<br>';
                      this.points.forEach(function(point) {
                        tip += point.series.name + '：<b>' + point.y + '</b><br>';
                      });
                      tip += (promotion ? '促销信息 : <b>' + promotion + '</b>' : '');
                      return tip;
                    }
                  },
                  yAxis: [{
                    // lineWidth: 1,
                    title: {
                      style: {
                        color: '#dd515c'
                      },
                      text: '价格'
                    },
                    min: 0
                  }, {
                    // lineWidth: 1,
                    opposite: true,
                    title: {
                      style: {
                        color: '#22beef'
                      },
                      text: '销量'
                    },
                    min: 0
                  }],
                  series: [{}, {
                    yAxis: 1
                  }]
                },
                el: '.line'
              });

              this.childViews = {

                line: this.line
              };
            }
          });

          this.grid = new Grid({

            modelProperty: {
              url1: App.dataURL.product_special_grid_in,
              url2: App.dataURL.product_special_grid_allOut,
              url3: App.dataURL.product_special_grid_out,
              param: $.extend({
                limit: 10,
                begin: datepicker.getBegin(),
                end: datepicker.getEnd(),
                sort: 'sold_month'
              }, publicParam),
              cols1: [{
                col: 'seller_name',
                name: '店铺名称'
              }, {
                col: 'name',
                name: '产品名称'
              }, {
                col: 'nowPrice',
                name: '当前价格',
                format: 'number',
                sortable: true
              }, {
                col: 'sold_month',
                name: '总销量',
                format: 'number',
                sortable: true
              }],
              cols2: [{
                col: 'cname',
                name: '电商名称'
              }, {
                col: 'nowPrice',
                name: '当前价格',
                format: 'number',
                sortable: true
              }]
            },
            el: '.grid',
            template: '#template_product_special_grid',

            show: function() {
              var cid = this.parent.model.data.cid;
              if (cid) {
                if (cid in {
                    'Mtmall': '',
                    'Mtaobao': ''
                  }) {
                  this.model.url = this.model.url1;
                } else {
                  this.model.url = this.model.url3;
                }
                this.model.cols = this.model.cols1;
              } else {
                this.model.url = this.model.url2;
                this.model.cols = this.model.cols2;
              }
              this.model.setParam({
                start: 0,
                cid: cid
              });
            }
          });

          this.store = new StoreView();

          this.childViews = {

            grid: this.grid
          };
        }
      });

      var fullLine = new Line({
        modelProperty: {
          url: App.dataURL.product_fullLine,
          param: $.extend({
            begin: datepicker.getBegin(),
            end: datepicker.getEnd()
          }, publicParam),
          group: 'time',
          cols: [{
            col: 'avgPrice',
            name: '平均价格'
          }, {
            col: 'salesVolume',
            name: '销量'
          }],
          getSingleData: function(col, store) {
            var res = {
              y: store[col]
            };
            if (col === 'avgPrice') {
              res.maxPrice = store.maxPrice;
              res.minPrice = store.minPrice;
            }
            return res;
          }
        },
        option: {
          tooltip: {
            formatter: function() {
              var tip = this.x + '<br>';
              this.points.forEach(function(point) {
                tip += point.series.name + '：<b>' + point.y + '</b><br>';
              });
              tip += '全网最高价：<b>' + (this.points[0].point.maxPrice || '-') + '</b><br>';
              tip += '全网最低价：<b>' + (this.points[0].point.minPrice || '-') + '</b><br>';
              return tip;
            }
          },
          yAxis: [{
            // lineWidth: 1,
            title: {
              style: {
                color: '#dd515c'
              },
              text: '平均价格'
            },
            min: 0
          }, {
            // lineWidth: 1,
            opposite: true,
            title: {
              style: {
                color: '#22beef'
              },
              text: '销量'
            },
            min: 0
          }],
          series: [{}, {
            yAxis: 1
          }]
        },
        el: '.full-line'
      });

      this.detail = new DetailView();
      this.special = new SpecialView();

      this.childViews = {

        datepicker: datepicker,

        detail: this.detail,

        fullLine: fullLine,

        special: this.special
      };

      this.listen(datepicker.model, 'change', function(data) {
        this.detail.tags.model.setParam({
          begin: data.begin,
          end: data.end
        });
        fullLine.model.setParam({
          begin: data.begin,
          end: data.end
        });
        var dateParam = {
          begin: data.begin,
          end: data.end
        };
        if (!this.special.store.isActive) {
          this.special.grid.model.setParam(dateParam);
        } else {
          this.special.store.line.model.setParam(dateParam);
        }
      });
    }
  });

  return View;
});