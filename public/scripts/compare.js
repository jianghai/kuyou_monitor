define([
  'jquery',
  'trunk',
  'base',
  'datepicker',
  'select',
  'search',
  'grid',
  'dialog',
  'scripts/compare.setting',
  'dropdown'
], function($, Trunk, base, Datepicker, Select, Search, Grid, Dialog, Setting) {

  var View = Trunk.View.extend({

    events: {
      'click .set': 'onSet',
      'keypress .price': 'onPricePress',
      'paste .price': 'onPricePaste',
      'submit .form-price': 'onPriceSearch',
      'click .checkbox': 'onChecked',
      'change .size': 'onSizeChange',
      'change .brand': 'onBrandChange',
    },

    onSet: function() {
      this.setting.show();
    },

    onPricePress: function(e) {
      var charCode = (e.which) ? e.which : event.keyCode;
      if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
    },

    onPricePaste: function(e) {
      var data = e.originalEvent.clipboardData.getData('text/plain');
      if (isNaN(data)) {
        return false;
      }
    },

    onPriceSearch: function(e) {
      var target = $(e.target);
      var prices = [];
      target.find('.price').each(function() {
        // if (!this.value) {
        //     this.focus();
        //     return false;
        // }
        prices.push(this.value);
      });
      this.trigger('price:search', prices);
      return false;
    },

    onChecked: function(e) {
      var target = $(e.target);
      var li = target.closest('li');
      this.selected.el.addClass('unfold');
      var _data = this.list.model.data.stores[target.attr('data-index')];
      if (target.prop('checked')) {
        if (this.selected.selectItems.length() === 4) {
          e.preventDefault();
          return;
        }
        li.addClass('active');
        this.selected.selectItems.add(_data);
      } else {
        li.removeClass('active');
        this.selected.selectItems.each(function(model) {
          if (model.data.id === _data.id) {
            model.remove();
            return false;
          }
        });
      }
    },

    afterRender: function() {
      if (!(App.category in {
          'tv': '',
          'phone': ''
        })) {
        this.$('.size').hide().prev().hide();
      }
    },

    onSizeChange: function(e) {
      this.list.model.setParam({
        size: e.target.value,
        start: 0
      });
    },

    onBrandChange: function(e) {
      this.list.model.setParam({
        brand: e.target.value,
        start: 0
      });
    },

    init: function() {

      var _this = this;

      var datepicker = new Datepicker();

      this.size = new Select({
        modelProperty: {
          defaults: {
            title: '全部尺寸'
          },
          url: App.dataURL.compare_size,
          param: {
            category: App.category
          }
        },
        el: '.size'
      });

      this.brand = new Select({
        modelProperty: {
          defaults: {
            title: '全部品牌'
          },
          url: App.dataURL.compare_brand,
          param: {
            category: App.category
          }
        },
        el: '.brand'
      });

      var search = new Search({
        modelProperty: {
          defaults: {
            placeholder: '输入产品型号'
          }
        }
      });

      var list = this.list = new Grid({
        modelProperty: {
          url: App.dataURL.compare_list,
          param: {
            limit: 12,
            category: App.category,
            begin: datepicker.getBegin(),
            end: datepicker.getEnd()
          }
        },
        beforeRender: function() {
          var _selected = {};
          selected.selectItems.each(function(item) {
            _selected[item.data.id] = 1;
          });
          this.model.set({
            selected: _selected
          });
        },
        el: '.list',
        template: '.template-list'
      });

      var SelectedView = Trunk.View.extend({

        model: Trunk.Model.extend(),

        el: '.selected',

        template: '.template-selected',

        events: {
          'click .toggle': 'onToggle',
          'click .btn-compare': 'onCompare',
          'click .remove-all': 'onRemoveAll',
        },

        onCompare: function(e) {
          var target = $(e.target);
          if (target.hasClass('disabled')) return false;
          var _items = this.selectItems.toArray();
          localStorage.setItem('compareItems', JSON.stringify(_items));
        },

        onRemoveAll: function() {
          this.selectItems.clear();
          return false;
        },

        onToggle: function() {
          this.el.toggleClass('unfold');
        },

        addOne: function(model) {
          var view = new this.SelectItemView({
            model: model
          });
          this.$('.items').append(view.el);
        },

        onItemRemove: function(model) {
          list.$('[data-id="' + model.data.id + '"]').removeClass('active')
            .find('.checkbox').prop('checked', false);
        },

        onItemsChange: function() {
          this.$('.btn-compare').toggleClass('disabled', this.selectItems.length() < 2);
        },

        afterRender: function() {
          var _this = this;
          if (Trunk.get && Trunk.get.selected) {
            $.each(JSON.parse(localStorage.getItem('compareItems')), function() {
              _this.selectItems.add(this);
            });
          }
        },

        init: function() {

          var SelectItem = Trunk.Model.extend();

          var SelectItems = Trunk.Models.extend({

            model: SelectItem
          });

          this.SelectItemView = Trunk.View.extend({

            model: SelectItem,

            tag: 'div',

            className: 'select-item',

            template: '#template_compare_select_item',

            events: {
              'click .remove': 'onRemove'
            },

            onRemove: function(e) {
              this.model.remove();
            },

            init: function() {
              this.render();
            }
          });

          var selectItems = this.selectItems = new SelectItems();

          this.listen(selectItems, 'add', this.addOne);
          this.listen(selectItems, 'reduce', this.onItemRemove);
          this.listen(selectItems, 'change', this.onItemsChange);
        }
      });

      var selected = this.selected = new SelectedView();

      this.setting = new Setting();

      new Dialog({
        wapper: $('#content'),
        modelProperty: {
          defaults: {
            icon: 'gear',
            title: '对比设置'
          }
        },
        child: this.setting
      });

      // Events
      this.listen(datepicker.model, 'change', function(data) {
        search.model.reset();
        list.model.setParam($.extend(data, {
          start: 0
        }));
      });

      this.on('price:search', function(prices) {
        // search.model.reset();

        prices[0] && prices[1] && prices[0] > prices[1] && prices.reverse();
        list.model.setParam({
          minPrice: prices[0],
          maxPrice: prices[1],
          start: 0
        });
      });

      search.on('search', function(key) {
        // _this.$('.select').each(function() {
        //     this.selectedIndex = 0;
        // });
        // _this.$('.price').val('');
        list.model.setParam({
          modelType: key,
          start: 0
        });
      });

      this.childViews = {

        datepicker: datepicker,

        size: this.size,

        brand: this.brand,

        search: search,

        list: this.list,

        selected: selected
      };
    }
  });

  return View;
});