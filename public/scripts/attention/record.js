define([
  'jquery',
  'trunk',
  'base',
  'grid',
  'dialog'
], function($, Trunk, base, Grid, Dialog) {

  return Grid.extend({
    modelProperty: {
      url: App.dataURL.attention_record,
      cols: [{
        col: 'category',
        name: '产品名称'
      }, {
        col: 'modelType',
        name: '产品型号'
      }, {
        col: 'brand',
        name: '产品品牌'
      }, {
        col: 'alarmType',
        name: '平台名称'
      }, {
        col: 'alarmNum',
        name: '店铺名称'
      }, {
        col: 'insertTime',
        name: '报警时间'
      }, {
        col: 'updateTime',
        name: '报警类型'
      }, {
        col: 'updateTime',
        name: '报警阈值'
      }, {
        col: 'updateTime',
        name: '报警值'
      }, {
        col: 'updateTime',
        name: '报警邮箱'
      }]
    },
    events: {
      'click .show-emails': 'onShowEmails'
    },
    onShowEmails: function(e) {
      this.emails.model.setParam('id', $(e.target).attr('data-id'));
    },
    template: '.template-grid',
    init: function() {
      this.emails = new base.View({
        modelProperty: {
          url: App.dataURL.attention_emails
        },
        tag: 'div',
        className: 'attention-emails scroll',
        template: '#template_attention_list_emails'
      });

      this.dialog = new Dialog({
        wapper: $('#content'),
        modelProperty: {
          defaults: {
            title: '报警邮箱'
          }
        },
        child: this.emails
      });
    }
  });
});