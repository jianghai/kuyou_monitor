define([
  'jquery',
  'trunk',
  'base',
  'autocomplete',
  'search',
  'grid',
  'popover',
  'error',
  'tab',
  'dropdown',
], function($, Trunk, base, Autocomplete, Search, Grid, popover, error) {

  var Form = base.View.extend({

    modelProperty: {
      defaults: {
        shops: {},
        email: []
      },
      url: App.dataURL.attention_edit,
      rules: {
        modelType: function(v, data) {
          if (!v || !data.name) return '请选择产品';
        },
        shops: function(v, data) {
          if ($.isEmptyObject(v) && !data.url) return '至少添加一个店铺或URL';
        },
        url: function(v) {
          if (v) {
            var urls = v.split('\n');
            for (var i = 0; i < urls.length; i++) {
              if (urls[i] && !/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/.test(urls[i])) {
                return '包含不正确的URL';
              }
            }
          }
        },
        alarmPriceOpen: function(v, data) {
          if (!v && !data.alarmSalesOpen) return '至少选择一个报警';
        },
        alarmPrice: function(v, data) {
          if (data.alarmPriceOpen && !v) return '请输入阈值';
          if (v && !/^\d+$/.test(v)) return '请输入大于0的整数';
        },
        alarmSales: function(v, data) {
          if (data.alarmSalesOpen && !v) return '请输入阈值';
          if (v && !/^\d+$/.test(v)) return '请输入大于0的整数';
        },
        email: function(v) {
          if (!v.length) return '请绑定报警邮箱';
        }
      },
      // Data format manager
      map: {
        alarmPriceOpen: 'alarmType'
      },
      validate: function(data) {
        var self = this;
        var rules = this.rules;
        var error = this.error = {};
        var isValid = true;
        rules && $.each(data, function(k, v) {
          if (rules[k]) {
            var res = rules[k].call(self, v, data);
            if (typeof res === 'string' || res === false) {
              isValid = false;
              error[self.map[k] || k] = res;
            }
          }
        });
        return isValid;
      }
    },

    onValidate: function(data) {
      var self = this;
      $.each(data, function(k, v) {
        var field = self.$('.field.' + (self.model.map[k] || k));
        field.length && field.removeClass('error').find('.error').text('');
      });
    },

    onInvalid: function() {
      var self = this;
      $.each(this.model.error, function(k, v) {
        var field = self.$('.field.' + k).addClass('error');
        v && field.find('.error').text(v);
      });
    },

    show: function() {
      if (Trunk.get && Trunk.get.id) {
        this.isEdit = true;
        this.model.param = {
          id: Trunk.get.id
        };
        this.model.fetch();
      } else {
        this.render();
      }
    },

    el: '.form',

    template: '.template-form',

    events: {
      'click .tao': 'onTaoClick',
      'click .tao-out': 'onTaoOutClick',
      'click .back-tao-out': 'onBackTaoOutClick',
      'click .select-shop': 'onShopSelect',
      'submit': 'onSubmit',
      'click .back': 'back',
    },

    onSubmit: function(e) {
      var self = this;
      var formData = $(e.target).serializeObject();
      formData.alarmPriceOpen || (formData.alarmPriceOpen = '');
      formData.modelType || (formData.modelType = '');
      formData.name || (formData.name = '');
      formData.email || (formData.email = []);
      formData.shops = this.model.data.shops;
      formData.category = App.category;
      
      if (this.model.set(formData)) {
        formData.shops = JSON.stringify(formData.shops);
        this.isEdit && (formData.id = this.model.data.id);
        $.post(App.dataURL.attention_add_update, formData, function(res) {
          if (res.success) self.back();
        });
      }
      return false;
    },

    back: function() {
      history.back();
    },

    onShopSelect: function(e) {
      var target = $(e.target);
      var cid = this.cid;
      var iid = target.val();
      if (target.prop('checked')) {
        if (Object.keys(this.model.data.shops).length === 20) {
          error.model.reset({
            tip: '未选择产品'
          });
          return;
        }
        this.model.data.shops[cid + iid] = {
          cid: cid,
          iid: iid,
          nowPrice: target.attr('data-price'),
          sold_month: target.attr('data-sold'),
        };
      } else {
        delete this.model.data.shops[cid + iid];
      }
      this.model.set({
        shops: this.model.data.shops
      }, {validate: false});
    },

    afterRender: function() {
      var self = this;
      this.$grid = this.$('.grid');
      this.$shops = this.$('.shops');
      this.$count = this.$('.shops-selected .count');
      popover.bind({
        wapper: $('#content'),
        triggers: this.$('.add-emails'),
        content: this.addEmails.el,
        triggerEvent: 'click',
        layout: 'horizontal',
        onTrigger: function(el) {
          self.addEmails.render();
        }
      });
      if (this.isEdit) {
        this.preview.model.reset({
          isEdit: true,
          img: this.model.data.img,
          name: this.model.data.name,
          modelType: this.model.data.modelType,
          brand: this.model.data.brand
        });
        this.emails.render(this.model.data.email);
      }
    },

    onTaoClick: function(e) {
      if (!this.model.data.brand) return;
      this.cid = $(e.target).attr('data-cid');
      this.tao.model.setParam({
        start: 0,
        cid: this.cid
      });
      this.$grid.html(this.tao.el);
    },

    onTaoOutClick: function() {
      if (!this.model.data.brand) return;
      this.taoOut.model.setParam({
        start: 0,
        brand: this.model.data.brand,
        modelType: this.model.data.modelType
      });
      this.$grid.html(this.taoOut.el);
    },

    onBackTaoOutClick: function() {
      this.$shops.removeClass('tao-out-shops');
      this.onTaoOutClick();
    },

    init: function() {

      var self = this;

      this.cid = 'Mtmall';

      this.autocomplete = new Autocomplete({
        result: {
          modelProperty: {
            url: App.dataURL.attention_edit_autocomplete,
            param: {
              category: App.category
            }
          },
          searchParam: 'search',
          template: '#template_attention_edit_autocomplete',
        },
        el: '.autocomplete'
      });

      this.preview = new base.View({
        modelProperty: {
          url: App.dataURL.attention_edit_preview
        },
        template: '#template_attention_edit_preview',
        el: '.preview',
        silent: true
      });

      var gridParse = function(res) {
        res.stores && $.each(res.stores, function() {
          if (self.model.data.shops[self.cid + this.iid]) {
            this.checked = true;
          }
        });
      }

      this.tao = new Grid({
        modelProperty: {
          url: App.dataURL.attention_edit_tao,
          param: {
            category: App.category,
            limit: 6,
            sort: 'sold_month'
          },
          parse: gridParse
        },
        tag: 'div',
        className: 'grid-tao',
        template: '#template_attention_edit_tao'
      });

      this.taoOut = new Grid({
        modelProperty: {
          url: App.dataURL.attention_edit_taoOut,
          param: {
            category: App.category,
            limit: 6,
            sort: 'nowPrice'
          }
        },
        events: {
          'click button': 'onButtonClick'
        },
        onButtonClick: function(e) {
          this.trigger('expand', $(e.target).attr('data-cid'));
        },
        tag: 'div',
        className: 'grid-taoOut',
        template: '#template_attention_edit_taoOut'
      });

      this.taoOutShops = new Grid({
        modelProperty: {
          url: App.dataURL.attention_edit_taoOutShops,
          param: {
            category: App.category,
            limit: 6,
            sort: 'nowPrice'
          },
          parse: gridParse
        },
        tag: 'div',
        className: 'grid-taoOutShops',
        template: '#template_attention_edit_taoOutShops'
      });

      this.emails = new Trunk.View({
        el: '.emails',
        silent: true,
        template: '#template_attention_edit_email',
        events: {
          'click .remove': 'onRemove'
        },
        onRemove: function(e) {
          $(e.currentTarget).parent().remove();
        },
        render: function(data) {
          this.$('.list').append(this.template(data));
        }
      });

      this.addEmails = new Trunk.View({
        modelProperty: {
          validate: function(data) {
            this.error = null;
            var self = this;
            if (!data.emails) {
              this.error = '请输入邮箱';
            } else if (data.emails.length > 50) {
              this.error = '邮箱不能超过50个';
            } else {
              $.each(data.emails, function(i, v) {
                if (!/\S+@\S+\.\S+/.test(v)) {
                  self.error = '包含不合法的邮箱';
                  return false;
                }
              });
            }
            return !this.error;
          }
        },
        tag: 'div',
        className: 'add-emails',
        template: '#template_attention_edit_addEmails',
        events: {
          'click .add': 'onAdd',
          'click .cancel': 'close',
        },
        onAdd: function() {
          var val = this.$('textarea').val();
          if (this.model.set({
            emails: val && val.split('\n')
          })) {
            this.trigger('add', this.model.data);
            this.close();
          }
        },
        close: function() {
          popover.close();
        },
        onValidate: function() {
          this.$('.error').empty();
        },
        onInvalid: function() {
          // this.$('.textarea').focus();
          this.$('.error').text(this.model.error);
        }
      });

      
      // Events
      this.model.on('change:shops', function(shops) {
        self.$count.text(Object.keys(shops).length);
      });
      this.autocomplete.on('select', function(data) {
        data && self.preview.model.setParam('id', data.id);
      });

      this.preview.on('render:after', function() {
        
        if (this.model.data.success == 0) return;

        if (!this.model.data.isEdit) {
          self.cid = 'Mtmall';
          self.$shops.removeClass('tao-out-shops')
            .find('.tmall').addClass('active').siblings().removeClass('active');

          self.model.set({
            shops: {}
          }, {validate: false});
        }

        self.model.set(this.model.data);

        self.tao.model.setParam({
          start: 0,
          brand: this.model.data.brand,
          modelType: this.model.data.modelType,
          cid: self.cid
        });
        self.$grid.html(self.tao.el);
      });

      this.taoOut.on('expand', function(cid) {
        self.cid = cid;
        self.$shops.addClass('tao-out-shops');
        self.taoOutShops.model.setParam({
          start: 0,
          brand: self.brand,
          modelType: self.modelType,
          cid: cid
        });
        self.$grid.html(self.taoOutShops.el);
      });

      this.addEmails.on('add', function(data) {
        self.emails.render(data.emails);
      });

      this.childViews = {
        autocomplete: this.autocomplete,
        preview: this.preview,
        emails: this.emails
      }
    }

  });

  var View = Trunk.View.extend({

    init: function() {
      this.childViews = {
        form: new Form()
      }
    }
  });

  return View;
});