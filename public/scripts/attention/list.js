define([
  'jquery',
  'trunk',
  'base',
  'select',
  'search',
  'scripts/attention/record'
], function($, Trunk, base, Select, Search, Record) {

  var View = Trunk.View.extend({

    events: {
      'change .brand': 'onBrandChange',
      'change .alarm-type': 'onAlarmChange',
      'change .platform': 'onPlatformChange',
    },

    onPlatformChange: function(e) {
      this.grid.model.setParam({
        cid: e.target.value
      });
    },

    onAlarmChange: function(e) {
      this.grid.model.setParam({
        alarmType: e.target.value
      });
    },

    onBrandChange: function(e) {
      this.grid.model.setParam({
        brand: e.target.value
      });
    },

    init: function() {

      var self = this;

      this.brand = new Select({
        modelProperty: {
          defaults: {
            title: '全部品牌'
          },
          param: {
            category: App.category
          },
          url: App.dataURL.attention_brand
        },
        el: '.brand'
      });

      this.platform = new Select({
        modelProperty: {
          defaults: {
            title: '全部平台'
          },
          param: {
            category: App.category
          },
          url: App.dataURL.attention_platform
        },
        template: '.template-select',
        el: '.platform'
      });

      this.searchByShop = new Search({
        modelProperty: {
          defaults: {
            placeholder: '按店铺搜索'
          }
        },
        el: '.by-shop'
      });

      this.searchByType = new Search({
        modelProperty: {
          defaults: {
            placeholder: '按型号搜索'
          }
        },
        el: '.by-type'
      });

      this.grid = new Record({
        modelProperty: {
          param: {
            category: App.category
          }
        },
        el: '.grid'
      });

      this.searchByShop.on('search', function(key) {
        self.grid.model.setParam({
          sellerName: key
        });
      });

      this.searchByType.on('search', function(key) {
        self.grid.model.setParam({
          modelType: key
        });
      });
      
      this.childViews = {
        brand: this.brand,
        platform: this.platform,
        searchByShop: this.searchByShop,
        searchByType: this.searchByType,
        grid: this.grid
      }
    }

  });

  return View;
});