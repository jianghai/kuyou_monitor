define([
  'jquery',
  'trunk',
  'base',
  'datepicker',
  'search',
  'dialog',
  'scripts/checking.quadrant',
  'grid',
], function($, Trunk, base, Datepicker, Search, Dialog, quadrantGraph, Grid) {

  var View = Trunk.View.extend({

    events: {
      'click .export': 'onExport'
    },

    onExport: function(e) {
      var target = $(e.currentTarget);
      target[0].href = App.dataURL[target.attr('data-type') === 'quadrant' ? 'checking_export' : 'ranking_export'] + '?' + $.param({
        category: App.category,
        begin: this.datepicker.getBegin(),
        end: this.datepicker.getEnd()
      });
    },

    init: function() {

      this.datepicker = new Datepicker();

      var search = new Search({
        modelProperty: {
          defaults: {
            placeholder: '在此输入产品型号'
          }
        }
      });

      var Quadrant = base.Model.extend({

        param: {
          category: App.category,
          begin: this.datepicker.getBegin(),
          end: this.datepicker.getEnd()
        },

        url: App.dataURL.checking_quadrant,

        parse: function(res) {
          var _res = [
            [],
            [],
            [],
            []
          ];
          var _xCount = 0;
          var _xDivider = [];
          var _yRange = [0, 0];
          $.each(res.stores, function() {
            if (_yRange[0] < this.y) {
              _yRange[0] = this.y;
            }
            if (_yRange[1] > this.y) {
              _yRange[1] = this.y;
            }
            if (_xCount < .8) {
              _xCount += this.x;
              _res[this.y >= 0 ? 0 : 3].push(this);
            } else {
              if (!_xDivider[0]) {
                _xDivider[0] = this.x;
                _xDivider[1] = Math.max(_res[0].length && _res[0][_res[0].length - 1].x, _res[3].length && _res[3][_res[3].length - 1].x);
              }

              _res[this.y >= 0 ? 1 : 2].push(this);
            }
          });
          return {
            series: _res,
            xRange: [0, res.stores[0].x],
            xDivider: _xDivider,
            yRange: _yRange
          };
        },

        onFetch: function(res) {
          this.reset(this.parse(res));
        }
      });

      var QuadrantView = base.View.extend({

        model: Quadrant,

        el: '.quadrant',

        render: function() {
          var _this = this;

          this.el.empty();
          // debugger;
          quadrantGraph($.extend({
            el: this.el[0],
            mouseover: function(d) {
              // console.log(this);
              // console.log(d3.event);
              _this.tip.position(d3.event);
              _this.tip.model.reset(d);
            },
            mousemove: function() {
              _this.tip.position(d3.event);
            },
            mouseout: function() {
              _this.tip.close();
            },
            click: function(d) {
              // if (d3.event.defaultPrevented) return;
              
              _this.detail.model.reset(d);
            }
          }, this.model.data));
        },

        init: function() {

          var TipView = Trunk.View.extend({

            model: Trunk.Model.extend(),

            tag: 'div',

            template: '#template_checking_tip',

            className: 'tooltip',

            afterRender: function() {
              this.el.addClass('open')
            },

            position: function(e) {
              var _position = [e.x + 10, e.y + 10];
              var _size = [this.el.width(), this.el.height()];
              if (_position[0] + _size[0] + 20 > this.screen[0]) {
                _position[0] -= _size[0] + 30;
              }
              if (_position[1] + _size[1] + 20 > this.screen[1]) {
                _position[1] -= _size[1] + 30;
              }
              // console.log(_position);
              this.el.css({
                left: _position[0],
                top: _position[1]
              });
            },

            close: function() {
              this.el.removeClass('open');
            },

            init: function() {
              var _window = $(window);
              this.screen = [_window.width(), _window.height()];
              $('body').append(this.el);
            }
          });

          var DetailView = Trunk.View.extend({

            model: Trunk.Model.extend(),

            tag: 'div',

            template: '#template_checking_detail',

            className: 'quadrant-detail',

            init: function() {

              var parent = this;

              var List = base.Model.extend({

                url: App.dataURL.checking_quadrant_detail,

                onFetch: function(res) {
                  this.reset(res);
                },

                param: {
                  category: App.category
                }
              });

              var ListView = base.View.extend({

                model: List,

                el: '.list',

                template: '#template_checking_detail_list',

                show: function() {
                  this.model.setParam({
                    brand: parent.model.data.brand,
                    modelType: parent.model.data.modelType
                  });
                }
              });

              this.childViews = {
                list: new ListView()
              };
            }
          });

          this.tip = new TipView();
          this.detail = new DetailView();

          new Dialog({
            wapper: $('#content'),
            modelProperty: {
              defaults: {
                title: '产品参数摘要'
              }
            },
            child: this.detail
          });
        }
      });

      var quadrant = new QuadrantView();

      var grid = new Grid({

        modelProperty: {

          url: App.dataURL.ranking_grid,

          param: {
            category: App.category,
            limit: 15,
            begin: this.datepicker.getBegin(),
            end: this.datepicker.getEnd(),
            sort: 'salesVolume'
          },

          cols: [{
            col: 'modelType',
            name: '产品型号'
          }, {
            col: 'brand',
            name: '产品品牌'
          }, {
            col: 'avgPrice',
            name: '当前全网平均价格',
            format: 'number',
            sortable: true
          }, {
            col: 'salesVolume',
            name: '总销量',
            format: 'number',
            sortable: true
          }]
        },

        el: '.grid',

        template: '.template-grid'
      });

      this.listen(this.datepicker.model, 'change', function(data) {
        search.model.reset();
        quadrant.model.setParam(data);
        grid.model.setParam({
          start: 0,
          begin: data.begin,
          end: data.end
        });
      });

      search.on('search', function(key) {
        if (!key) {
          d3.selectAll('.circles circle').classed('searched', false);
          return;
        }
        key = key.toUpperCase();
        d3.selectAll('.circles circle').each(function() {
          d3.select(this).classed('searched', this.getAttribute('data-model').indexOf(key) !== -1);
        });

        grid.model.setParam({
          modelType: key
        });
      });

      this.childViews = {

        datepicker: this.datepicker,

        search: search,

        quadrant: quadrant,

        grid: grid
      };
    }
  });

  return View;
});