define([
  'jquery',
  'trunk',
  'base',
  'select',
  'search',
  'grid',
  'confirm',
  'error',
  'dialog',
  'scripts/attention/record',
], function($, Trunk, base, Select, Search, Grid, confirm, error, Dialog, Record) {

  var View = Trunk.View.extend({

    afterRender: function() {
      this.$('.btn.add').attr('href', '#' + App.topUrl + '/attention/add');
    },

    events: {
      'click .clear-all': 'onClearAll',
      'change .brand': 'onBrandChange',
      'change .alarm-type': 'onAlarmChange',
    },

    onAlarmChange: function(e) {
      this.grid.model.setParam({
        alarmType: e.target.value
      });
    },

    onBrandChange: function(e) {
      this.grid.model.setParam({
        brand: e.target.value
      });
    },

    onClearAll: function() {
      this.grid.removeAll();
    },

    init: function() {

      var self = this;

      this.brand = new Select({
        modelProperty: {
          defaults: {
            title: '全部品牌'
          },
          param: {
            category: App.category
          },
          url: App.dataURL.attention_brand
        },
        el: '.brand'
      });

      this.search = new Search({
        modelProperty: {
          defaults: {
            placeholder: '按型号搜索'
          }
        }
      });

      this.grid = new Grid({
        modelProperty: {

          url: App.dataURL.attention_list,

          param: {
            category: App.category,
            limit: 15
          },

          cols: [{
            col: 'category',
            name: '品类'
          }, {
            col: 'modelType',
            name: '产品型号'
          }, {
            col: 'brand',
            name: '产品品牌'
          }, {
            col: 'alarmType',
            name: '报警类型'
          }, {
            col: 'alarmNum',
            name: '报警次数'
          }, {
            col: 'insertTime',
            name: '添加时间'
          }, {
            col: 'updateTime',
            name: '最后修改时间'
          }]
        },

        events: {
          'click .remove': 'onRemove',
          'click .show-record': 'onShowRecord',
          'click .toggle-all': 'onToggleAll'
        },

        onShowRecord: function(e) {
          this.record.model.setParam('id', $(e.target).attr('data-id'));
          return false;
        },

        onToggleAll: function(e) {
          var isChecked = e.target.checked;
          this.$('input').prop('checked', isChecked);
        },

        onRemove: function(e) {
          var self = this;
          var id = $(e.currentTarget).attr('data-id');
          confirm.model.reset({
            title: '确定删除吗？'
          });
          confirm.callback = function() {
            self.remove([id]);
          }
        },

        remove: function(ids) {
          var self = this;
          $.post(App.dataURL.attention_list_delete, {
            ids: ids
          }, function(res) {
            if (res.success) {
              self.model.setParam({
                start: 0
              });
            }
          });
        },

        getCheckes: function() {
          return this.$('tbody :checked').map(function() {
            return this.value;
          }).get();
        },

        removeAll: function() {
          var checks = this.getCheckes();
          if (checks.length) {
            var self = this;
            confirm.model.reset({
              title: '确定删除吗？'
            });
            confirm.callback = function() {
              self.remove(checks);
            }
          } else {
            error.model.reset({
              tip: '未选择产品'
            });
          }
        },

        el: '.grid',

        template: '.template-grid',

        init: function() {
          this.record = new Record({
            modelProperty: {
              url: App.dataURL.attention_product_record,
              param: {
                limit: 5,
                category: App.category
              }
            },
            tag: 'div',
            className: 'attention-record module',
            template: '#template_attention_record'
          });
          this.dialog = new Dialog({
            wapper: $('#content'),
            modelProperty: {
              defaults: {
                title: '报警记录'
              }
            },
            child: this.record
          })
        }
      });

      this.search.on('search', function(key) {
        self.grid.model.setParam({
          modelType: key
        });
      });

      this.childViews = {
        brand: this.brand,
        search: this.search,
        grid: this.grid
      }
    }

  });

  return View;
});