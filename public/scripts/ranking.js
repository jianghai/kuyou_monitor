define([
  'datepicker',
  'search',
  'grid',
], function(Datepicker, Search, Grid) {

  var View = Trunk.View.extend({

    events: {
      'click .export': 'onExport'
    },

    onExport: function(e) {
      e.currentTarget.href = App.dataURL.ranking_export + '?' + $.param({
        category: App.category,
        begin: this.datepicker.getBegin(),
        end: this.datepicker.getEnd()
      });
    },

    init: function() {

      var _this = this;

      this.datepicker = new Datepicker();

      var search = new Search({
        modelProperty: {
          defaults: {
            placeholder: '在此输入产品型号'
          }
        }
      });

      this.childViews = {

        datepicker: this.datepicker,

        search: search,

        grid: new Grid({

          modelProperty: {

            url: App.dataURL.ranking_grid,

            param: {
              category: App.category,
              limit: 15,
              begin: this.datepicker.getBegin(),
              end: this.datepicker.getEnd(),
              sort: 'salesVolume'
            },

            cols: [{
              col: 'modelType',
              name: '产品型号'
            }, {
              col: 'brand',
              name: '产品品牌'
            }, {
              col: 'avgPrice',
              name: '当前全网平均价格',
              format: 'number',
              sortable: true
            }, {
              col: 'salesVolume',
              name: '总销量',
              format: 'number',
              sortable: true
            }]
          },

          el: '.grid',

          template: '.template-grid'
        })
      };

      this.datepicker.on('change', function(data) {
        search.model.reset();
        _this.childViews.grid.model.setParam({
          start: 0,
          begin: data.begin,
          end: data.end
        });
      });

      search.on('search', function(key) {
        _this.childViews.grid.model.setParam({
          modelType: key
        });
      });
    }
  });

  return View;
});