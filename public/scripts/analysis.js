define([
  'jquery',
  'trunk',
  'datepicker',
  'highcharts.line',
  'highcharts.pie',
  'grid',
  'scripts/analysis.circles',
  'components/tags',
  'tab',
], function($, Trunk, Datepicker, Line, Pie, Grid, Circles, Tags) {

  var View = Trunk.View.extend({

    init: function() {

      this.datepicker = new Datepicker();

      var publicParam = {
        category: App.category,
        begin: this.datepicker.getBegin(),
        end: this.datepicker.getEnd()
      };

      var MarketSizeView = Trunk.View.extend({

        el: '.market-size',

        type: 'salesVolume',

        events: {
          'click .tab-item': 'onTab',
          'click .export': 'onExport',
        },

        onTab: function(e) {
          this.type = $(e.target).attr('data-type');
          var _line = this.childViews.line;
          _line.model.cols[0].name = this.type === 'salesVolume' ? '销量' : '成交额';
          _line.model.setParam({
            type: this.type,
          });
        },

        onExport: function(e) {
          e.currentTarget.href = App.dataURL.analysis_marketSize_export + '?' + $.param({
            type: this.type,
            category: App.category,
            begin: this.datepicker.getBegin(),
            end: this.datepicker.getEnd()
          });
        },

        init: function() {

          var _this = this;

          this.childViews = {

            line: new Line({
              modelProperty: {
                url: App.dataURL.analysis_marketSize_line,
                param: $.extend({
                  type: this.type
                }, publicParam),
                group: 'done_date',
                cols: [{
                  col: 'sales',
                  name: '销量'
                }]
              },
              el: '.line'
            })
          };
        }
      });

      var BrandRankingView = Trunk.View.extend({

        el: '.brand-ranking',

        template: '.template-brand-ranking',

        type: 'salesVolume',

        events: {
          'click .tab-item': 'onTab',
          'click .export': 'onExport'
        },

        onTab: function(e) {
          this.type = $(e.target).attr('data-type');
          var _pie = this.childViews.pie;
          var _grid = this.childViews.grid;
          _pie.model.name = (this.type === 'salesVolume' ? '销量' : '成交额') + '占比';
          _grid.model.cols[1].name = (this.type === 'salesVolume' ? '销量' : '成交额') + '占比';
          _pie.model.setParam({
            type: this.type
          });
          _grid.model.setParam({
            type: this.type
          });

          this.childViews.circles.model.setParam({
            type: this.type
          });
        },

        onExport: function(e) {
          e.currentTarget.href = App.dataURL.analysis_brandRanking_export + '?' + $.param({
            type: this.type,
            category: App.category,
            begin: this.datepicker.getBegin(),
            end: this.datepicker.getEnd()
          });
        },

        afterRender: function() {
          this.good.el = this.$('.good-content');
          this.bad.el = this.$('.poor-content');
        },

        init: function() {

          var _this = this;

          this.childViews = {

            pie: new Pie({

              modelProperty: {

                url: App.dataURL.analysis_brandRanking_pie,

                param: $.extend({
                  limit: 10,
                  type: this.type
                }, publicParam),

                name: '销量占比'
              },

              el: '.pie'
            }),

            grid: new Grid({

              modelProperty: {

                url: App.dataURL.analysis_brandRanking_grid,

                param: $.extend({
                  limit: 10,
                  type: this.type
                }, publicParam),

                cols: [{
                  col: 'brand',
                  name: '品牌名称'
                }, {
                  col: 'market_size',
                  name: '销量占比',
                  format: 'percent'
                }]
              },

              el: '.grid',

              template: '#template_analysis_brandRanking_grid'
            }),

            circles: new Circles({

              modelProperty: {

                url: App.dataURL.analysis_brandRanking_circles,

                param: $.extend({
                  limit: 10,
                  type: this.type
                }, publicParam),
              },

              el: '.circles',

              template: '#template_analysis_brandRanking_circles'
            })
          };

          this.good = new Tags({

            modelProperty: {

              url: App.dataURL.analysis_brandRanking_tag,

              param: $.extend({
                type: 'good',
                limit: 10
              }, publicParam),

              colorRange: ['#ffb980', '#e60012'],

              sizeRange: [12, 18]
            }
          });

          this.bad = new Tags({

            modelProperty: {

              url: App.dataURL.analysis_brandRanking_tag2,

              param: $.extend({
                type: 'bad',
                limit: 10
              }, publicParam),

              colorRange: ['#8e9cbc', '#0fabad'],

              sizeRange: [12, 18]
            }
          });

          var tagsFlush = function(brand) {
            _this.$('.brand-current').text(brand);
            _this.good.model.setParam({
              brand: brand
            });
            _this.bad.model.setParam({
              brand: brand
            });
          };

          this.childViews.circles.model.on('reset', function(data) {
            tagsFlush(data.stores[0].brand);
          });

          this.childViews.circles.on('select', function(brand) {
            window.scrollTo(0, document.body.scrollHeight);
            tagsFlush(brand);
          });

          
        }
      });

      this.marketSize = new MarketSizeView();
      this.brandRanking = new BrandRankingView();
      
      this.listen(this.datepicker.model, 'change', function(data) {
        var dateParam = {
          begin: data.begin,
          end: data.end
        };

        this.marketSize.childViews.line.model.setParam(dateParam);

        $.extend(this.brandRanking.good.model.param, dateParam);
        $.extend(this.brandRanking.bad.model.param, dateParam);
        $.each(this.brandRanking.childViews, function() {
          this.model.setParam(dateParam);
        });
      });

      this.childViews = {

        datepicker: this.datepicker,

        marketSize: this.marketSize,

        brandRanking: this.brandRanking
      };
    }
  });

  return View;
});