define([
  'jquery',
  'trunk',
  'datepicker',
  'grid',
  'tab',
], function($, Trunk, Datepicker, Grid) {


  var View = Trunk.View.extend({

    events: {
      'click .tab-item': 'onTab',
      'click .export': 'onExport'
    },

    type: 'salesVolume',

    onTab: function(e) {
      this.type = $(e.target).attr('data-type');
      var _grid = this.childViews.grid;
      _grid.model.cols[1].name = (this.type === 'salesVolume' ? '销量' : '成交额') + '占比';
      _grid.model.setParam({
        start: 0,
        type: this.type
      });
    },

    onExport: function(e) {
      e.currentTarget.href = App.dataURL.analysis_detail_export + '?' + $.param({
        type: this.type,
        category: App.category,
        begin: datepicker.getBegin(),
        end: datepicker.getEnd()
      });
    },

    init: function() {

      this.datepicker = new Datepicker();

      this.grid = new Grid({
        modelProperty: {

          url: App.dataURL.analysis_detail_grid,

          param: {
            category: App.category,
            type: this.type,
            limit: 15,
            begin: this.datepicker.getBegin(),
            end: this.datepicker.getEnd(),
            sort: 'market_size'
          },

          cols: [{
            col: 'brand',
            name: '品牌名称'
          }, {
            col: 'market_size',
            name: '销量占比',
            sortable: true,
            format: 'percent'
          }, {
            col: 'praise',
            name: '好评数',
            sortable: true,
            format: 'number'
          }, {
            col: 'badReview',
            name: '差评数',
            sortable: true,
            format: 'number'
          }]
        },

        el: '.grid',

        template: '.template-grid'
      });

      this.childViews = {

        datepicker: this.datepicker,

        grid: this.grid
      };

      this.listen(this.datepicker.model, 'change', function(data) {
        this.grid.model.setParam({
          start: 0,
          begin: data.begin,
          end: data.end
        });
      });
    }
  });

  return View;
});