define([
  'jquery',
  'trunk',
  'base',
  'tab',
  'pin'
], function($, Trunk, base) {

  var View = Trunk.View.extend({

    events: {
      'click .filter .btn': 'onTab',
      'click .roll-trigger': 'onRoll',
    },

    onTab: function(e) {
      var target = $(e.currentTarget);
      this.list.el.addClass('notransition').css('top', 0);
      this.list.model.setParam('param', target.text());
    },

    onRoll: function(e) {
      this.list.el.removeClass('notransition');
      var self = this;
      if (this.clicked) return;
      this.clicked = true;
      var isDown = $(e.currentTarget).hasClass('down');
      if (isDown) {
        this.index += this.limit;
        this.overflow.css('height', Math.min(this.count - this.index, this.limit) * 159 + 1);
      } else {
        this.index -= this.limit;
        this.overflow.css('height', this.limit * 159 + 1);
      }
      this.list.el.css('top', (isDown ? '-' : '+') + '=' + this.limit * 159);
      setTimeout(function() {
        self.clicked = false;
      }, 500);

      this.$('.up').prop('disabled', this.index === 0);
      this.$('.down').prop('disabled', this.count - this.index <= this.limit);
    },

    index: 0,

    limit: 12,

    afterRender: function() {
      this.overflow = this.$('.overflow');
    },

    init: function() {

      var self = this;

      this.list = new base.View({
        modelProperty: {
          url: App.dataURL.analysis_trend_list,
          param: {
            param: 'CPU核数'
          },
          parse: function(res) {
            $.each(res.stores, function(i) {
              this.color = App.color[i % 12];
            });
          }
        },
        el: '.list',
        template: '.template_list'
      });

      this.list.model.on('sync', function(res) {
        var count = res.stores.length;
        this.view.el.css('top', -self.index * 159);
        self.overflow.css('height', Math.min(count - self.index, self.limit) * 159 + 1);
        self.$('.down').prop('disabled', count - self.index < self.limit);
        self.count = count;
      });

      this.childViews = {
        list: this.list
      };
    }
  });

  return View;
});