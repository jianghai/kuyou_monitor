## 安装本地开发环境

```sh
# 安装依赖包（服务器端）
$ npm install

# 安装依赖包（浏览器端）
$ grunt install

# 启动服务
$ node-dev bin/www
```



## 切换运行模式

```sh
# 开发模式
$ grunt dev

# 线上模式
$ grunt pro
```



## 打包上线

```sh
$ grunt
```



## 说明

1. \*.preprocess.\* 为预处理文件，自动处理线上和开发环境的不同配置，配合 ``grunt watch`` 使用;
2. 数据接口配置在 public/app.preprocess.js 下;
3. _java 目录是提给给java环境下的模板等文件，打包后自动生成;


